import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import TodoApp from './TodoApp/container/TodoApp';
import {Provider} from 'react-redux';
import {store} from './TodoApp/TodoStore/TodoAppStore';

ReactDOM.render(
    <Provider store={store}>
			<TodoApp />
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
