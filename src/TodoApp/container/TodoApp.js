import React, {Component} from 'react';
import {connect} from 'react-redux';
import './_TodoApp.scss';
import {AppHeader} from '../components/TodoAppHeader/TodoAppHeader';
import {Sidebar} from '../components/Sidebar/Sidebar';
import {MainContent} from '../components/MainContent/MainContent';
import {Note} from '../components/Modal/Modal';
import * as TodoAction from '../actions/TodoAction';
import {bindActionCreators} from 'redux'

class TodoApp extends Component {
  render() {
    const {todos, actions} = this.props;
    return (
      <div className="App">
        <header>
          <AppHeader actions={actions}/>
        </header>
        <Sidebar todos={todos} actions={actions}/>
        <MainContent todos={todos} actions={actions}/>
        <Note todos={todos} actions={actions}/>
			</div>
		);
	}

}

const mapStateToProps = state => {
 return {todos: state};
}

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(TodoAction, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp);
