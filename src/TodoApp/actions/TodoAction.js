export const toggleSidebar = () => {
  return {type: 'TOGGLE_SIDEBAR'}
}

export const switchTask = taskId => {
  return {
    type: 'SWITCH_TASK',
    taskId
  }
}

export const createTask = (taskLists, taskId, todoLists) => {
  return {
    type: 'CREATE_TASK',
    taskLists,
    taskId,
    todoLists
  }
}

export const createTodo = (todoLists, todoId, todoContent) => {
  return {
    type: 'CREATE_TODO',
    todoLists,
    todoId,
    todoContent
  }
}

export const createNotes = notes => {
  return {
    type: "CREATE_NOTES",
    notes
  }
}

export const enableModal = (todoId, todoContent) => {
  return {
    type: 'ENABLE_MODAL',
    todoId,
    todoContent
  }
}

export const disableModal = () => {
  return {type: 'DISABLE_MODAL'}
}

export const removeTask = (taskLists, todoLists, taskId) => {
  return {
    type: 'REMOVE_TASK',
    taskLists,
    todoLists,
    taskId
  }
}