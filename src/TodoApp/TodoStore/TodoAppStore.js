import {createStore} from 'redux';
import todoApp from '../reducers/TodoReducer';

export let store = createStore(todoApp);

export function getTaskLists () {
  return store.getState().taskLists;
};

export const getTodoLists = task => {
  const taskLists = getTaskLists();
  for (let i in taskLists) {
    if (taskLists[i].taskId === task) {
      store.getState().todoLists = taskLists[i].todo;
    }
  }
}

export const updateTaskLists = todoLists => {
  const taskLists = getTaskLists();
  for (let i in taskLists) {
    if (taskLists[i].taskId === store.getState().taskId) {
      store.getState().todo = todoLists;
    }
  }
}

export const getNotes = (todoId, taskId) => {
  const todoLists = store.getState().todoLists;
  for (let i in todoLists) {
    if (todoLists[i].todoId === todoId) {
      store.getState().notes = todoLists[i].note;
    }
  }
}