const todos = {
  toggle: true,
  modal: false,
  taskLists: [{
    taskId: 'task1',
    taskContent: 'Today',
    todo: []
  }],
  todoLists: [],
  notes: {},
  taskId: "task1",
  todoId: "",
  todoContent: ""
}

const todoApp = (state = todos, action) => {
  switch (action.type) {
    case 'TOGGLE_SIDEBAR': 
      return Object.assign({
        ...state,
        toggle: !state.toggle
      });
    case 'SWITCH_TASK':{
      let todoLists;
      state.taskLists.map(task => {
        if (task.taskId === action.taskId) {
          todoLists = task.todo;
        }
      });
      return Object.assign({
        ...state,
        taskId: action.taskId,
        todoLists: todoLists
      });
    }
    case 'CREATE_TASK':
      return Object.assign({ 
        ...state,
        taskLists: action.taskLists,
        taskId: action.taskId,
        todoLists: action.todoLists
      });
    case 'CREATE_TODO':
      return Object.assign({
        ...state,
        todoId: action.todoId,
        todoContent: action.todoContent,
        ...state.taskLists.map(task => {
          if (task.taskId === action.taskId) {
            task.todo.push(action.todoLists);
            return Object.assign ({todo: task.todo});
          }
        })
      });
    case 'CREATE_NOTES':
      return Object.assign({
        ...state,
        notes: action.notes,
        ...state.taskLists.map(task => {
          if (task.taskId === action.taskId) {
            task.todo.map(todo => {
              if(todo.todoId === state.todoId) {
                todo.note = action.notes;
              }
            })
          }
        })
      });
    case 'ENABLE_MODAL':
      return Object.assign({
        ...state,
        modal: !state.modal,
        todoId: action.todoId,
        todoContent: action.todoContent,
        ...state.todoLists.map(todo => {
          if (todo.todoId === action.todoId) {
            state.notes = todo.note;
          }
        })
      });
      case 'DISABLE_MODAL':
        return (Object.assign({
          ...state,
          modal: !state.modal
        }));
      case 'REMOVE_TASK':
        return Object.assign({
          ...state,
          taskLists: action.taskLists,
          todoLists: action.todoLists,
          taskId: action.taskId
        });
    default:
      return state;
  }
}

export default todoApp;