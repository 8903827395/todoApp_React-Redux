import React from 'react';
import {getNotes} from '../../TodoStore/TodoAppStore';
import {Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import FaTrashO from 'react-icons/lib/fa/trash-o';

export class Note extends React.Component {
  PopulateNote = () => {
    getNotes(this.props.todoId, this.props.taskId);
    const notes = this.props.todos.notes;
    let comments = [];
    for(var field in notes) {
      comments.push(
        <div className="comment-content" key={notes[field]}>
          <input type="textbox" className="comment-name" value={notes[field]} readOnly />
          <span className="FaTrashO">
            <FaTrashO />
          </span>
        </div>
      );
    }
    return(
      <div className="comment">
        {comments}
      </div>
    );
  }

  toggle = () => {this.props.actions.disableModal()}

  addComment = e => {
    if (e.key === 'Enter') {
      const todoLists = this.props.todos.todoLists;
      for(let i in todoLists) {
        if(todoLists[i].todoId === this.props.todos.todoId) {
          getNotes(this.props.todos.todoId, this.props.todos.taskId);
          const notes = this.props.todos.notes;
          const key = 'comment' + (new Date()).getTime();
          notes[key] = e.target.value;
          this.props.actions.createNotes(notes);
        }
      }
      e.target.value = "";
    }
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.props.todos.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>{ this.props.todos.todoContent }</ModalHeader>
          <ModalBody>
            <div className="scratch-notes">
              <div className="add-comment">
                <input className="add-comment-input" type="textbox" placeholder="Add a Comment..." onKeyUp={this.addComment} />
              </div>
            </div>
            <this.PopulateNote />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
