import React, {Component} from 'react';
import _ from 'underscore';
import {FaTrash} from 'react-icons/lib/fa';
import {getTodoLists} from '../../TodoStore/TodoAppStore';

export class MainContent extends Component {
  onClick = (todo) => {
    this.props.actions.enableModal(todo.todoId, todo.todoContent);
  }

  addTodo = e => {
    if (e.key === 'Enter') {
      let taskLists = this.props.todos.taskLists;
      var todoLists;
      _.each(taskLists, task => {
        if (task.taskId === this.props.todos.taskId) {
          todoLists = task.todo;
        }
      });
       var newTodo = {
        todoId: 'todo' + (new Date()).getTime(),
        todoContent: e.target.value,
        note: {}
      }
      todoLists.push(newTodo);
      e.target.value = "";
      this.props.actions.createTodo(todoLists, newTodo.todoId, newTodo.todoContent);
    }
  }

  removeTodo = (todo, taskId) => {
    const todoLists = getTodoLists(taskId);
    for (let i in todoLists) {
      if (todoLists[i].todoId === todo.todoId) {
        todoLists.splice(i, 1);
      }
    }
    this.setState({
      todoLists: todoLists
    });
  }

  PopulateTodoList = () => {
    const todoLists = this.props.todos.todoLists;
    const todos = todoLists.map((todo) =>
      <div className="todo-main" key={todo.todoId}>
      <div className = "note"  id={todo.todoId} onClick={() => this.onClick(todo)}>
        <div className="note-content">
        <input type="textbox" className="note-name" value={todo.todoContent} readOnly />
        </div>
      </div>
      <span className="note-icons" onClick={() => this.removeTodo(todo, this.props.todos.taskId)}>
        <FaTrash />
      </span>
    </div>
    );
    return (
      <div className="todo" >
        <div className="add-note">
          <input className="addNote-input" type="textbox" placeholder="Add a note..." onKeyUp={this.addTodo} />
        </div>
        <div>
          {todos}
        </div>
      </div>
    );
  }
  
  render() {
    return(
      <div className="main">
        <this.PopulateTodoList />
      </div>
    );
  }
}
