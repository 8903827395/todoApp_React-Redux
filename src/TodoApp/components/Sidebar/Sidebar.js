import React from 'react';
import {FaPlus, FaList, FaTrashO} from 'react-icons/lib/fa';

export class Sidebar extends React.Component {
  onClick = e => {
    this.props.actions.switchTask(e.currentTarget.id);
  }

  removeTask = e => {
    const taskLists = this.props.todos.taskLists;
    for (let i in taskLists) {
      if (taskLists[i].taskId === e.currentTarget.id) {
        taskLists.splice(i, 1);
      }
    }
    this.props.actions.removeTask(taskLists, taskLists[0].todo, taskLists[0].taskId);
  }

  PopulateSidebar = () => {
    const taskLists = this.props.todos.taskLists;
    const tasks = taskLists.map(task =>
      <div className="tasks" key={task.taskId}>
        <div className="task" id={task.taskId} onClick={this.onClick}>
          <span className='icon'>
            <FaList />
          </span>
          <span className="taskContent">{task.taskContent}</span>
        </div>
        { (task.taskId !== 'task1') &&
          <div className='delete' id={task.taskId} onClick={this.removeTask}>
          <FaTrashO />
        </div>
        }
      </div>
    );
    return (
      <div className="sidebar-content">
        {tasks}
      </div>
    );
  }

  addTask = e => {
    if ( e.key === 'Enter') {
      let newTask = {
        taskId: 'task' + (new Date()).getTime(),
        taskContent: e.target.value, 
        todo: []
      }
      let taskLists = this.props.todos.taskLists;
      taskLists.push(newTask);
      e.target.value = "";
      this.props.actions.createTask(taskLists, newTask.taskId, newTask.todo);
    }
  }

  render() {
    return (
      <div className={this.props.todos.toggle ? 'fadeIn' : 'fadeOut'}>
        <div className='sidebar'>
          <div className='newTask'>
            <span className='icon'>
              <FaPlus />
            </span>
            <input className="newTask-input" placeholder="Create a New Task"  onKeyUp={this.addTask }/>
          </div>
          <this.PopulateSidebar />
        </div>
      </div>
    );
  }
}