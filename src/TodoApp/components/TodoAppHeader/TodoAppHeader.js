import React, {Component} from 'react';
import {FaBars, FaUser} from 'react-icons/lib/fa';

export class AppHeader extends Component {
  onClick = () => {
    this.props.actions.toggleSidebar();
  }

  render() {
    return (
      <div className="header" >
        <span className="menu" id="menu" onClick={this.onClick}>
          <FaBars />
        </span>
				<span className="appName">To-Do</span>
				<span className="user" id="user">
					<FaUser />
				</span>
				<span className="userName" id="userName">Kosalram</span>
			</div>
		);
	}
}
